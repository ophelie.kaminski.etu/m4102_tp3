#Programmation répartie - Pizzaland
##Représentation de la pizza

Une pizza possède un nom, un identifiant et plusieurs ingrédients. Sa représentation JSON prendra donc la forme :

	{
		"id" : "1",
		"name" : "ChevreMiel",
		"ingredients" : ["Chevre", "Miel", "Tomates"]
	}
	
Cette représentation correspond au DTO PizzaDTO, dont les attributs sont:
```java
long id;
String name;
List<Ingredient> ingredients = new ArrayList<Ingredient>();
```

Lors de la création, on ne connait pas l'identifiant puisqu'il est fourni par la base de données, la représentation JSON devient donc :

	{
		"name" : "ChevreMiel",
		"ingredients" : ["Chevre", "Miel", "Tomates"]
	}

Cette représentation correspond au DTO PizzaCreateDTO, dont les attibuts sont:
```java
private String name;
private List<Ingredient> ingredients;
```

##Définition de l'API

| Opération | URI         | Action réalisée                               | Retour                                        |
|:----------|:------------|:----------------------------------------------|:----------------------------------------------|
| GET       | /pizzas     | récupère l'ensemble des pizzas                | 200 et une liste de pizzas                    |
| GET       | /pizzas/{id}| récupère la pizza d'identifiant id            | 200 et la pizza                               |
|           |             |                                               | 404 si id est inconnu                         |
| GET       | /pizzas/{id}/name | récupère le nom de la pizza             | 200 et le nom de la pizza                     |
|           |             | d'identifiant id                              | 404 si id est inconnu                         |
| GET       | /pizzas/{id}/ingredients | récupère les ingrédients de la pizza d'identifiant id  | 200 et un tableau d'ingrédient |
|			|			  |                     						  | 404 si id est inconnu						  |
| POST      | /pizzas     | création d'une pizza                          | 201 et l'URI de la ressource créée + représentation |
|           |             |                                               | 400 si les informations ne sont pas correctes |
|           |             |                                               | 409 si la pizza existe déjà  (même nom)       |
| DELETE    | /pizzas/{id}| destruction de la pizza d'identifiant id      | 204 si l'opération a réussi                   |
|           |             |                                               | 404 si id est inconnu                         |
| DELETE	| /pizzas/{idP}/ingredients/{idI} | destruction de l'ingrédient d'identifiant idI sur la pizza idP | 209 si l'opération a réussi |
|			|			  |                                               | 404 si idI ou idP est inconnu				  |
