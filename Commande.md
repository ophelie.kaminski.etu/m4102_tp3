#Programmation répartie - Pizzaland
##Représentation d'une commande

Une commande est représentée par un nom, un prénom, un numéro de commande (identifiant) et une ou plusieurs pizzas. Sa représentation JSON prendra donc la forme :

	{
		"id" : "1",
		"firstName" : "Ophélie",
		"name" : "Kaminski",
		"pizzas" : ["ChevreMiel", "Chorizo"]
	}
	
Cette représentation correspond au DTO CommandeDTO.

Lors de la création, on ne connait pas l'identifiant puisqu'il est fourni par la base de données, la représentation JSON devient donc :

	{
		"firstName" : "Ophélie",
		"name" : "Kaminski",
		"pizzas" : ["ChevreMiel", "Chorizo"]
	}

Cette représentation correspond au DTO CommandeCreateDTO.

##Définition de l'API

| Opération | URI         | Action réalisée                               | Retour                                        |
|:----------|:------------|:----------------------------------------------|:----------------------------------------------|
| GET       | /commandes  | récupère l'ensemble des commandes             | 200 et une liste de commandes                 |
| GET       | /commandes/{id}| récupère la commande d'identifiant id      | 200 et la commande                            |
|           |             |                                               | 404 si id est inconnu                         |
| GET       | /commandes/{id}/pizzas | récupère les pizzas de la commande d'identifiant id | 200 et les pizzas commandées |
|           |             |                                               | 404 si id est inconnu                         |
| GET       | /commandes/{id}/name | récupère le nom de la commande d'identifiant id  | 200 et le nom de la personne qui a commandé |
|			|			  |                     						  | 404 si id est inconnu						  |
| POST      | /commandes  | création d'une commande                       | 201 et l'URI de la ressource créée + représentation |
|           |             |                                               | 400 si les informations ne sont pas correctes |
|           |             |                                               | 409 si la commande existe déjà  (même nom+prénom)|
| DELETE    | /commandes/{id}| destruction de la commande d'identifiant id| 204 si l'opération a réussi                   |
|           |             |                                               | 404 si id est inconnu                         |
| DELETE	| /commandes/{id}/pizzas/{idP} | destruction de la pizza d'identifiant idP dans la commande d'identifiant id | 209 si l'opération a réussi	|
|			|			  |                                               | 404 si id ou idP est inconnu				  |
