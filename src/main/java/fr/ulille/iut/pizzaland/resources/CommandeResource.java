package fr.ulille.iut.pizzaland.resources;

import java.net.URI;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;
import java.util.stream.Collectors;

import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;

import fr.ulille.iut.pizzaland.BDDFactory;
import fr.ulille.iut.pizzaland.beans.Commande;
import fr.ulille.iut.pizzaland.dao.CommandeDao;
import fr.ulille.iut.pizzaland.dto.CommandeCreateDto;
import fr.ulille.iut.pizzaland.dto.CommandeDto;

@Path("/commandes")
public class CommandeResource {
	
	private CommandeDao dao;
	private static final Logger LOGGER = Logger.getLogger(CommandeResource.class.getName());
	
	@Context
	public UriInfo uriInfo;
	
	public CommandeResource() {
		dao = BDDFactory.buildDao(CommandeDao.class);
	    dao.createTable();
	}

	@GET
	public List<CommandeDto> getAll(){
		 LOGGER.info("IngredientResource:getAll");
		 List<CommandeDto> c = dao.getAll().stream().map(Commande::toDto).collect(Collectors.toList());
		 return c;
	}
	
	@GET
	@Path("{id}")
	public CommandeDto getOneCommande(@PathParam("id") long id) {
		LOGGER.info("getOneCommande(" + id + ")");
		try {
	        Commande commande = dao.findById(id);
	        return Commande.toDto(commande);
	    }
	    catch ( Exception e ) {
	        throw new WebApplicationException(Response.Status.NOT_FOUND);
	    }
	}
	
	@POST
	public Response createCommande(CommandeCreateDto createDto) {
	    Commande existing = dao.findByName(createDto.getName(),createDto.getFirstName());
	    if ( existing != null ) {
	        throw new WebApplicationException(Response.Status.CONFLICT);
	    }
	    
	    try {
	        Commande cmd = Commande.fromCommandeCreateDto(createDto);
	        long id = dao.insert(cmd.getName(), cmd.getFirstName());
	        cmd.setId(id);
	        CommandeDto dto = Commande.toDto(cmd);
	        URI uri = uriInfo.getAbsolutePathBuilder().path("" + id).build();
	        return Response.created(uri).entity(dto).build();
	    }
	    catch ( Exception e ) {
	        e.printStackTrace();
	        throw new WebApplicationException(Response.Status.NOT_ACCEPTABLE);
	    }
	}

}
