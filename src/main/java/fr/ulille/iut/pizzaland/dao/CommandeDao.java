package fr.ulille.iut.pizzaland.dao;

import java.util.List;

import org.jdbi.v3.sqlobject.config.RegisterBeanMapper;
import org.jdbi.v3.sqlobject.statement.GetGeneratedKeys;
import org.jdbi.v3.sqlobject.statement.SqlQuery;
import org.jdbi.v3.sqlobject.statement.SqlUpdate;
import org.jdbi.v3.sqlobject.transaction.Transaction;

import fr.ulille.iut.pizzaland.beans.Commande;

public interface CommandeDao {
	
	@SqlUpdate("CREATE TABLE IF NOT EXISTS commandes (id INTEGER PRIMARY KEY, name VARCHAR NOT NULL, firstName VARCHAR NOT NULL)")
	void createTableCmd();
	
	@SqlUpdate("CREATE TABLE IF NOT EXISTS commandesPizzas (idC INTEGER, idP INTEGERCONSTRAINT pk_commandesPizzas PRIMARY KEY (idC,idP))")
	void createTableCmdPz();
	
	@Transaction
	default void createTable() {
		createTableCmd();
		//createTableCmdPz();
	}
	
	@SqlUpdate("DROP TABLE IF EXISTS commandes")
	void dropTableCmd();
	
	@SqlUpdate("DROP TABLE IF EXISTS commandesPizzas")
	void dropTableCmdPz();
	
	@Transaction
	default void dropTable() {
		dropTableCmd();
		//dropTableCmdPz();
	}
	  
	@SqlUpdate("INSERT INTO commandes (name, firstName) VALUES (:name, :firstName)")
	@GetGeneratedKeys
	long insert(String name, String firstName);
	
	  @SqlQuery("SELECT * FROM commandes")
	  @RegisterBeanMapper(Commande.class)
	  List<Commande> getAll();
	  
	  @SqlQuery("SELECT * FROM commandes WHERE id = :id")
	  @RegisterBeanMapper(Commande.class)
	  Commande findById(long id);

	  @SqlQuery("SELECT * FROM commandes WHERE name = :name AND firstName = :firstName")
	  @RegisterBeanMapper(Commande.class)
	  Commande findByName(String name, String firstName);



}
