package fr.ulille.iut.pizzaland.beans;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import fr.ulille.iut.pizzaland.dto.CommandeCreateDto;
import fr.ulille.iut.pizzaland.dto.CommandeDto;
import fr.ulille.iut.pizzaland.dto.PizzaDto;

public class Commande {
	
	private long id;
	private String firstName;
	private String name;
	private List<Pizza> pizzas;
	
	public Commande() {
		
	}
	
	public Commande(long id, String firstName, String name, List<Pizza> pizzas) {
		this.id = id;
		this.firstName = firstName;
		this.name = name;
		this.pizzas = pizzas;
	}

	public void setName(String name) {
		this.name=name;
	}

	public void setId(long id) {
		this.id = id;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public void setPizzas(List<Pizza> pizzas) {
		this.pizzas = pizzas;
	}

	public long getId() {
		return id;
	}

	public String getFirstName() {
		return firstName;
	}

	public String getName() {
		return name;
	}

	public List<Pizza> getPizzas() {
		return pizzas;
	}
	
	public void addPizza(Pizza p) {
		if(this.pizzas == null) {
			this.pizzas = new ArrayList<Pizza>();
		}
		this.pizzas.add(p);
	}
	
	public static CommandeDto toDto(Commande c) {
		CommandeDto dto = new CommandeDto();
		dto.setFirstName(c.getFirstName());
		dto.setId(c.getId());
		dto.setName(c.getName());
		dto.setPizzas(c.getPizzas());
		return dto;
	}
	
	public static Commande fromDto(CommandeDto dto) {
		Commande c = new Commande(dto.getId(),dto.getFirstName(),dto.getName(),dto.getPizzas());
		return c;
	}
	
	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
	        return true;
		}
	    if (obj == null) {
	        return false;
	    }
	    if (getClass() != obj.getClass()) {
	        return false;
	    }
	    Commande other = (Commande) obj;
	    if (id != other.id) {
	        return false;
	    }
	    if (name == null) {
	        if (other.name != null) {
	            return false;
	        }
	    } else if(firstName == null) {
	    	if(other.firstName != null) {
	    		return false;
	    	}
	    } else if (!name.equals(other.name) || !firstName.equals(other.firstName)) {
	        return false;
	    }
	    return true;
	}
	
	@Override
	public String toString() {
		return "Commande : [ id : " + this.getId() + ", firstName : "+this.getFirstName()+", name : "+this.getName() +
				", pizzas : "+Arrays.deepToString(this.getPizzas().toArray()) +"]";
	}

	public static Commande fromCommandeCreateDto(CommandeCreateDto createDto) {
		Commande c = new Commande();
		c.setFirstName(createDto.getFirstName());
		c.setName(createDto.getName());
		c.setPizzas(createDto.getPizzas());
		return c;
	}

}
