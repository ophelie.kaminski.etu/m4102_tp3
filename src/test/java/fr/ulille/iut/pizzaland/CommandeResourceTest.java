/**
 * 
 */
package fr.ulille.iut.pizzaland;

import static org.junit.Assert.*;

import java.util.List;

import javax.ws.rs.client.Entity;
import javax.ws.rs.core.Application;
import javax.ws.rs.core.GenericType;
import javax.ws.rs.core.Response;

import org.glassfish.jersey.test.JerseyTest;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import fr.ulille.iut.pizzaland.beans.Commande;
import fr.ulille.iut.pizzaland.dao.CommandeDao;
import fr.ulille.iut.pizzaland.dto.CommandeCreateDto;
import fr.ulille.iut.pizzaland.dto.CommandeDto;

public class CommandeResourceTest extends JerseyTest{
	
	private CommandeDao dao;

	@Override
	protected Application configure() {
		BDDFactory.setJdbiForTests();
		return new ApiV1();
	}
	
	@Before
	public void setEnvUp() {
	    dao = BDDFactory.buildDao(CommandeDao.class);
	    dao.createTable();
	}
	@After
	public void tearEnvDown() throws Exception {
	   dao.dropTable();
	}
	
	@Test
	public void getEmptyList() {
		Response response = target("/commandes").request().get();
	    assertEquals(Response.Status.OK.getStatusCode(), response.getStatus());
	    List<CommandeDto> commandes;
	    commandes = response.readEntity(new GenericType<List<CommandeDto>>(){});
	    assertEquals(0, commandes.size());
	}
	

	 @Test
	 public void testGetExistingCommande() {
	   Commande commande = new Commande(1,"Ophelie","Kaminski",null);
	   Response response = target("/commandes/1").request().get();
	   assertEquals(Response.Status.OK.getStatusCode(), response.getStatus());
	   Commande result  = Commande.fromDto(response.readEntity(CommandeDto.class));
	   assertEquals(commande, result);
	}
	 
	 @Test
	 public void testGetExistingCommandeBDD() {
	     Commande commande = new Commande();
	     commande.setName("Kaminski");
	     commande.setFirstName("Ophelie");
	     long id = dao.insert(commande.getName(),commande.getFirstName());
	     commande.setId(id);
	     Response response = target("/commandess/" + id).request().get();
	     assertEquals(Response.Status.OK.getStatusCode(), response.getStatus());
	     Commande result = Commande.fromDto(response.readEntity(CommandeDto.class));
	     assertEquals(commande, result);
	 }

	 @Test
	 public void testGetNotExistingCommande() {
	   Response response = target("/commandes/125").request().get();
	   assertEquals(Response.Status.NOT_FOUND.getStatusCode(),response.getStatus());
	 }
	 
	 @Test
	 public void testCreateCommande() {
	     CommandeCreateDto createDto = new CommandeCreateDto();
	     createDto.setName("Kaminski");
	     createDto.setFirstName("Ophelie");
	     Response response = target("/commandes")
	             .request()
	             .post(Entity.json((createDto)));
	     assertEquals(Response.Status.CREATED.getStatusCode(), response.getStatus());
	     CommandeDto returnedEntity = response.readEntity(CommandeDto.class);
	     assertEquals(target("/commandes/" +
	         returnedEntity.getId()).getUri(), response.getLocation());
	     assertEquals(returnedEntity.getName(), createDto.getName());
	     assertEquals(returnedEntity.getFirstName(), createDto.getFirstName());
	 }



}
